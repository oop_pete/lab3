import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        int starType;
        int n;
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            starType = sc.nextInt();
            if(starType == 5){
                System.out.println("Bye bye!!!");
                break;
            }
            switch(starType){
            case 1:
                System.out.print("Please input number: ");
                n = sc.nextInt();
                for(int i = 0; i < n; i++){
                    for(int j = 0; j < n; j++){
                        if(j<=i){
                            System.out.print("*");
                        }else{
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
                break;
            case 2:
                System.out.print("Please input number: "); 
                n = sc.nextInt();
                for(int i = 0; i < n; i++){
                    for(int j = 0; j < n; j++){
                        if(i <= n - j - 1){
                            System.out.print("*");
                        }else{
                            System.out.print(" ");
                        }
                     }
                    System.out.println();
                }
                break;
            case 3: 
                System.out.print("Please input nimber: ");
                n = sc.nextInt();
                for(int i = 0; i < n; i++){
                    for(int j = 0; j < n; j++){
                        if(i <= j){
                            System.out.print("*");
                        }else{
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
                break;
            case 4:
                System.out.print("Please input number: ");
                n = sc.nextInt();
                for(int i =0; i < n; i++){
                    for(int j = 0; j <= n - i; j++){
                        System.out.print(" ");
                    }
                    for(int j = 0; j <= i; j++){
                        System.out.print("*");
                    }
                    System.out.println();
                }
                break;
            default:
                System.out.println("Error: Please input number between 1-5");
                break;
            }
        }
        sc.close();
    }
}
