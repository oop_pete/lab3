public class Problem17 {
    public static void main(String[] args) {
        for(int i = 1; i <= 5; i++){
            int count =1;
            for(int j = 1; j <= 5-i; j++){
                System.out.print(" ");
                count++;
            }
            for(int j = 1; j <= i; j++){
                System.out.print(count);
                count++;
            }
            System.out.println();
        }
    }
}
